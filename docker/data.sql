INSERT INTO troles VALUES (1, 'ROLE_ADMIN');
INSERT INTO troles VALUES (2, 'ROLE_USER');

INSERT INTO tusers (username, password, email, role_id)
VALUES ('admin', '$2a$10$ryYNbOp.0AigfayTHeaMuudYHHFNDXXgPiJVSHvcsmxROkbfT6X.y', 'admin@licenta.fmi', 1);