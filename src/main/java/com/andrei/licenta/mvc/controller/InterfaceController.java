package com.andrei.licenta.mvc.controller;

import com.andrei.licenta.service.InterfaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * MIT License
 * <p>
 * Copyright (c) 2018 Andrei Popescu
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
@Controller
public class InterfaceController {

    @Autowired
    private InterfaceService interfaceService;

    @RequestMapping("/")
    public String redirectEntryPoint(Model model) {
        if (interfaceService.checkAnonymous()) {
            return "redirect:/login";
        }
        return interfaceService.redirectToProfile(model);
    }

    @RequestMapping("/logout")
    public String logout() {
        interfaceService.clearAuth();
        return "redirect:/login";
    }

    @RequestMapping("/login")
    public String showLogin(Model model) throws IOException {
        if (!interfaceService.checkAnonymous()) {
            return interfaceService.redirectToProfile(model);
        }

        interfaceService.checkGlobalErrors(model);

        return "login";
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String showRegister(Model model) {
        if (!interfaceService.checkAnonymous()) {
            return interfaceService.redirectToProfile(model);
        }

        return "register";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String doRegister(HttpServletRequest request, Model model) {
        if (!interfaceService.checkAnonymous()) {
            return interfaceService.redirectToProfile(model);
        }

        return interfaceService.registerUser(request, model);
    }

    @RequestMapping("/u/{username}")
    public String displayProfile(Model model, @PathVariable("username") String username) throws Exception {
        if (interfaceService.checkAnonymous()) {
            return "redirect:/login";
        }

        return interfaceService.displayProfile(model, username);
    }

    @RequestMapping(value = "/description", method = RequestMethod.POST)
    public String editDescription(HttpServletRequest request, Model model) throws Exception {
        if (interfaceService.checkAnonymous()) {
            return "redirect:/login";
        }

        return interfaceService.editProfileDescription(request, model);
    }

    @RequestMapping(value = "/tweet", method = RequestMethod.POST)
    public String postTweet(HttpServletRequest request, Model model) throws Exception {
        if (interfaceService.checkAnonymous()) {
            return "redirect:/login";
        }

        return interfaceService.postTweet(request, model);
    }

    @RequestMapping(value = "/tweet/like", method = RequestMethod.POST)
    public String likeTweet(HttpServletRequest request, Model model) throws Exception {
        if (interfaceService.checkAnonymous()) {
            return "redirect:/login";
        }

        return interfaceService.likeTweet(request, model);
    }

    @RequestMapping(value = "/user/follow", method = RequestMethod.POST)
    public String followUser(HttpServletRequest request, Model model) throws Exception {
        if (interfaceService.checkAnonymous()) {
            return "redirect:/login";
        }

        return interfaceService.followUser(request, model);
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public String searchWebsite(HttpServletRequest request, Model model) throws Exception {
        if (interfaceService.checkAnonymous()) {
            return "redirect:/login";
        }

        return interfaceService.search(request, model);
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String showSearchPage(Model model) throws Exception {
        if (interfaceService.checkAnonymous()) {
            return "redirect:/login";
        }

        return interfaceService.showSearch(model);
    }
}
