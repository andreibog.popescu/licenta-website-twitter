package com.andrei.licenta.mvc.controller;

import com.andrei.licenta.auth.ApiAuthentication;
import com.andrei.licenta.db.dto.Follower;
import com.andrei.licenta.db.dto.User;
import com.andrei.licenta.mvc.model.*;
import com.andrei.licenta.service.UserService;
import com.andrei.licenta.utils.LogProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Objects;

@Controller
@RequestMapping("/api/user")
public class UserController implements LogProvider {

    private final UserService userService;
    private final ApiAuthentication apiAuthentication;

    @Autowired
    public UserController(UserService userService, ApiAuthentication apiAuthentication) {
        this.userService = userService;
        this.apiAuthentication = apiAuthentication;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ResponseEntity<GenericResponse> register(@RequestBody RegisterUser userDetails) throws IOException {
        GenericResponse registrationResponse = userService.registerUser(userDetails);

        return new ResponseEntityBuilder<GenericResponse>().setHttpStatus(HttpStatus.OK).responseBody(registrationResponse).build();
    }

    @RequestMapping(value = "/{username}", method = RequestMethod.GET)
    public ResponseEntity<GenericResponse> getUserProfile(HttpServletRequest request, @PathVariable("username") String username) throws IOException {
        GenericResponse auth = apiAuthentication.attemptAuthentication(request);
        if (auth.getResult().getCode().equals(ActionResultCode.AUTH_FAILURE)) {
            return new ResponseEntityBuilder<GenericResponse>().setHttpStatus(HttpStatus.UNAUTHORIZED).responseBody(auth).build();
        }

        GenericResponse myUserDetailsResponse = userService.getUserDetails(request.getHeader("username"));
        GenericResponse userDetailsResponse = userService.getUserDetails(username);

        if (userDetailsResponse.getResult().getCode() == ActionResultCode.OK) {
            User myUser = (User) myUserDetailsResponse.getDetails();
            User user = (User) userDetailsResponse.getDetails();

            boolean isFollowed = userService.checkIfFollowed(myUser, user);

            ((User) userDetailsResponse.getDetails()).setFollowed(isFollowed);
            return new ResponseEntityBuilder<GenericResponse>().setHttpStatus(HttpStatus.OK).responseBody(userDetailsResponse).build();
        } else {
            GenericResponse genericResponse = new GenericResponse<>(ActionResult.buildResult(ActionResultCode.ERROR, "This user cannot edit details for another user."), null);
            return new ResponseEntityBuilder<GenericResponse>().setHttpStatus(HttpStatus.UNAUTHORIZED).responseBody(genericResponse).build();
        }
    }

    @RequestMapping(value = "/{username}/followers", method = RequestMethod.GET)
    public ResponseEntity<GenericResponse> getUserFollowers(HttpServletRequest request, @PathVariable("username") String username) throws IOException {
        GenericResponse auth = apiAuthentication.attemptAuthentication(request);
        if (auth.getResult().getCode().equals(ActionResultCode.AUTH_FAILURE)) {
            return new ResponseEntityBuilder<GenericResponse>().setHttpStatus(HttpStatus.UNAUTHORIZED).responseBody(auth).build();
        }

        GenericResponse myUserDetailsResponse = userService.getUserDetails(request.getHeader("username"));
        GenericResponse userFollowersResponse = userService.getUserDetails(username);

        if (userFollowersResponse.getResult().getCode() == ActionResultCode.OK) {
            User myUser = (User) myUserDetailsResponse.getDetails();
            User user = (User) userFollowersResponse.getDetails();

            GenericResponse genericResponse = userService.getFollowers(myUser, user);

            return new ResponseEntityBuilder<GenericResponse>().setHttpStatus(HttpStatus.OK).responseBody(genericResponse).build();
        } else {
            GenericResponse genericResponse = new GenericResponse<>(ActionResult.buildResult(ActionResultCode.ERROR, "This user cannot edit details for another user."), null);
            return new ResponseEntityBuilder<GenericResponse>().setHttpStatus(HttpStatus.UNAUTHORIZED).responseBody(genericResponse).build();
        }
    }

    @RequestMapping(value = "/{username}", method = RequestMethod.PUT)
    public ResponseEntity<GenericResponse> getUserProfile(HttpServletRequest request, @PathVariable("username") String username, @RequestBody User user) throws IOException {
        GenericResponse auth = apiAuthentication.attemptAuthentication(request);
        if (auth.getResult().getCode().equals(ActionResultCode.AUTH_FAILURE)) {
            return new ResponseEntityBuilder<GenericResponse>().setHttpStatus(HttpStatus.UNAUTHORIZED).responseBody(auth).build();
        }

        GenericResponse myUserDetailsResponse = userService.getUserDetails(request.getHeader("username"));
        if (myUserDetailsResponse.getResult().getCode() == ActionResultCode.OK) {
            User foundUser = (User) myUserDetailsResponse.getDetails();

            if (foundUser.getUsername().equals(username)) {
                //Only edit description at this point
                foundUser.setDescription(user.getDescription());
                GenericResponse userResponse = userService.editUser(foundUser);
                return new ResponseEntityBuilder<GenericResponse>().setHttpStatus(HttpStatus.OK).responseBody(userResponse).build();
            } else {
                GenericResponse genericResponse = new GenericResponse<>(ActionResult.buildResult(ActionResultCode.ERROR, "This user cannot edit details for another user."), null);
                return new ResponseEntityBuilder<GenericResponse>().setHttpStatus(HttpStatus.UNAUTHORIZED).responseBody(genericResponse).build();
            }
        }

        return new ResponseEntityBuilder<GenericResponse>().setHttpStatus(HttpStatus.OK).responseBody(myUserDetailsResponse).build();
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public ResponseEntity<GenericResponse> searchUsers(HttpServletRequest request, @RequestParam(value = "value") String searchString) throws IOException {
        GenericResponse auth = apiAuthentication.attemptAuthentication(request);
        if (auth.getResult().getCode().equals(ActionResultCode.AUTH_FAILURE)) {
            return new ResponseEntityBuilder<GenericResponse>().setHttpStatus(HttpStatus.UNAUTHORIZED).responseBody(auth).build();
        }

        GenericResponse searchResult = userService.searchUsers(searchString);
        return new ResponseEntityBuilder<GenericResponse>().setHttpStatus(HttpStatus.OK).responseBody(searchResult).build();
    }

    @RequestMapping(value = "/{id}/follow", method = RequestMethod.POST)
    public ResponseEntity<GenericResponse> followUser(HttpServletRequest request, @PathVariable(value = "id") Long userId) throws IOException {
        GenericResponse auth = apiAuthentication.attemptAuthentication(request);
        if (auth.getResult().getCode().equals(ActionResultCode.AUTH_FAILURE)) {
            return new ResponseEntityBuilder<GenericResponse>().setHttpStatus(HttpStatus.UNAUTHORIZED).responseBody(auth).build();
        }

        GenericResponse myUserDetailsResponse = userService.getUserDetails(request.getHeader("username"));
        GenericResponse userToFollowResponse = userService.getUserDetails(userId);

        if (userToFollowResponse.getResult().getCode() == ActionResultCode.OK) {
            Follower followerObj = new Follower((User) myUserDetailsResponse.getDetails(), (User) userToFollowResponse.getDetails());
            GenericResponse followResponse = userService.followUser(followerObj);

            return new ResponseEntityBuilder<GenericResponse>().setHttpStatus(HttpStatus.OK).responseBody(followResponse).build();
        } else {
            return new ResponseEntityBuilder<GenericResponse>().setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR).responseBody(
                    new GenericResponseBuilder<>()
                            .addResult(ActionResult.buildResult(ActionResultCode.USER_FOLLOW_FAILURE, "User to follow not found."))
                            .build()
            ).build();
        }
    }

    @RequestMapping(value = "/{id}/unfollow", method = RequestMethod.POST)
    public ResponseEntity<GenericResponse> unfollowUser(HttpServletRequest request, @PathVariable(value = "id") Long userId) throws IOException {
        GenericResponse auth = apiAuthentication.attemptAuthentication(request);
        if (auth.getResult().getCode().equals(ActionResultCode.AUTH_FAILURE)) {
            return new ResponseEntityBuilder<GenericResponse>().setHttpStatus(HttpStatus.UNAUTHORIZED).responseBody(auth).build();
        }

        GenericResponse myUserDetailsResponse = userService.getUserDetails(request.getHeader("username"));
        GenericResponse userToFollowResponse = userService.getUserDetails(userId);

        if (userToFollowResponse.getResult().getCode() == ActionResultCode.OK) {
            Follower followerObj = new Follower((User) myUserDetailsResponse.getDetails(), (User) userToFollowResponse.getDetails());
            GenericResponse followResponse = userService.unFollowUser(followerObj);

            return new ResponseEntityBuilder<GenericResponse>().setHttpStatus(HttpStatus.OK).responseBody(followResponse).build();
        } else {
            return new ResponseEntityBuilder<GenericResponse>().setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR).responseBody(
                    new GenericResponseBuilder<>()
                            .addResult(ActionResult.buildResult(ActionResultCode.USER_UNFOLLOW_FAILURE, "User to unfollow not found."))
                            .build()
            ).build();
        }
    }

    @Profile("dev")
    @RequestMapping(value = "/testcleanup", method = RequestMethod.DELETE)
    public ResponseEntity deleteTestUsers(HttpServletRequest request){
        GenericResponse auth = apiAuthentication.attemptAuthentication(request);
        if (auth.getResult().getCode().equals(ActionResultCode.AUTH_FAILURE) && Objects.equals(request.getHeader("username"), "admin@licenta.fmi")) {
            return new ResponseEntityBuilder<GenericResponse>().setHttpStatus(HttpStatus.UNAUTHORIZED).responseBody(auth).build();
        }

        userService.deleteTestUsers();

        return new ResponseEntityBuilder<GenericResponse>().setHttpStatus(HttpStatus.OK).responseBody(
                new GenericResponseBuilder().addResult(new ActionResult(ActionResultCode.OK, "Test Users deleted.")).build()
        ).build();
    }
}
