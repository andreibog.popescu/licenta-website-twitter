package com.andrei.licenta.mvc.controller;

import com.andrei.licenta.auth.ApiAuthentication;
import com.andrei.licenta.db.dto.Tweet;
import com.andrei.licenta.db.dto.User;
import com.andrei.licenta.mvc.model.ActionResult;
import com.andrei.licenta.mvc.model.ActionResultCode;
import com.andrei.licenta.mvc.model.GenericResponse;
import com.andrei.licenta.mvc.model.ResponseEntityBuilder;
import com.andrei.licenta.service.TweetService;
import com.andrei.licenta.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.sql.Timestamp;
import java.time.Instant;

/**
 * MIT License
 * <p>
 * Copyright (c) 2018 Andrei Popescu
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
@Controller
@RequestMapping("/api/tweet")
public class TweetController {

    @Autowired
    private UserService userService;

    @Autowired
    private TweetService tweetService;

    @Autowired
    private ApiAuthentication apiAuthentication;

    @RequestMapping(value = "/{username}", method = RequestMethod.POST)
    public ResponseEntity<GenericResponse> addMyTweet(HttpServletRequest request, @PathVariable("username") String username, @RequestBody Tweet tweet) throws IOException {
        GenericResponse auth = apiAuthentication.attemptAuthentication(request);
        if (auth.getResult().getCode().equals(ActionResultCode.AUTH_FAILURE)) {
            return new ResponseEntityBuilder<GenericResponse>()
                    .setHttpStatus(HttpStatus.UNAUTHORIZED)
                    .responseBody(auth)
                    .build();
        }

        GenericResponse myUserDetailsResponse = userService.getUserDetails(request.getHeader("username"));
        if (myUserDetailsResponse.getResult().getCode() == ActionResultCode.OK) {
            User user = (User) myUserDetailsResponse.getDetails();

            if (user.getUsername().equals(username)) {
                tweet.setUser(user);
                tweet.setCreationDate(Timestamp.from(Instant.now()));
                GenericResponse tweetResponse = tweetService.addTweet(tweet);
                return new ResponseEntityBuilder<GenericResponse>().setHttpStatus(HttpStatus.OK).responseBody(tweetResponse).build();
            } else {
                GenericResponse genericResponse = new GenericResponse<>(ActionResult.buildResult(ActionResultCode.ERROR, "This user cannot post tweets for another user."), null);
                return new ResponseEntityBuilder<GenericResponse>()
                        .setHttpStatus(HttpStatus.UNAUTHORIZED)
                        .responseBody(genericResponse).build();
            }
        } else {
            GenericResponse genericResponse = new GenericResponse<>(ActionResult.buildResult(ActionResultCode.USERNAME_ERROR, "User not found."), null);
            return new ResponseEntityBuilder<GenericResponse>().setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR).responseBody(genericResponse).build();
        }
    }

    @RequestMapping(value = "/{username}", method = RequestMethod.GET)
    public ResponseEntity<GenericResponse> getMyTweets(HttpServletRequest request, @PathVariable("username") String username) throws IOException {
        GenericResponse auth = apiAuthentication.attemptAuthentication(request);
        if (auth.getResult().getCode().equals(ActionResultCode.AUTH_FAILURE)) {
            return new ResponseEntityBuilder<GenericResponse>().setHttpStatus(HttpStatus.UNAUTHORIZED).responseBody(auth).build();
        }

        GenericResponse userDetailsResponse = userService.getUserDetails(username);
        GenericResponse myUserDetailsResponse = userService.getUserDetails(request.getHeader("username"));
        if (userDetailsResponse.getResult().getCode() == ActionResultCode.OK) {
            User myUser = (User) myUserDetailsResponse.getDetails();
            User user = (User) userDetailsResponse.getDetails();

            GenericResponse tweetResponse = tweetService.getAllTweetsForUser(myUser, user);
            return new ResponseEntityBuilder<GenericResponse>().setHttpStatus(HttpStatus.OK).responseBody(tweetResponse).build();
        } else {
            GenericResponse genericResponse = new GenericResponse<>(ActionResult.buildResult(ActionResultCode.USERNAME_ERROR, "User not found."), null);
            return new ResponseEntityBuilder<GenericResponse>().setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR).responseBody(genericResponse).build();
        }
    }

    @RequestMapping(value = "/{id}/like", method = RequestMethod.POST)
    public ResponseEntity<GenericResponse> likeTweet(HttpServletRequest request, @PathVariable("id") Long tweetId) throws IOException {
        GenericResponse auth = apiAuthentication.attemptAuthentication(request);
        if (auth.getResult().getCode().equals(ActionResultCode.AUTH_FAILURE)) {
            return new ResponseEntityBuilder<GenericResponse>().setHttpStatus(HttpStatus.UNAUTHORIZED).responseBody(auth).build();
        }

        GenericResponse myUserDetailsResponse = userService.getUserDetails(request.getHeader("username"));
        if (myUserDetailsResponse.getResult().getCode() == ActionResultCode.OK) {
            Tweet tweet = new Tweet();
            tweet.setId(tweetId);

            User user = (User) myUserDetailsResponse.getDetails();
            tweet.setUser(user);

            GenericResponse genericResponse = tweetService.likeTweet(tweet);
            return new ResponseEntityBuilder<GenericResponse>().setHttpStatus(HttpStatus.OK).responseBody(genericResponse).build();
        } else {
            GenericResponse genericResponse = new GenericResponse<>(ActionResult.buildResult(ActionResultCode.USERNAME_ERROR, "User not found."), null);
            return new ResponseEntityBuilder<GenericResponse>().setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR).responseBody(genericResponse).build();
        }
    }

    @RequestMapping(value = "/{id}/unlike", method = RequestMethod.POST)
    public ResponseEntity<GenericResponse> unlikeTweet(HttpServletRequest request, @PathVariable("id") Long tweetId) throws IOException {
        GenericResponse auth = apiAuthentication.attemptAuthentication(request);
        if (auth.getResult().getCode().equals(ActionResultCode.AUTH_FAILURE)) {
            return new ResponseEntityBuilder<GenericResponse>().setHttpStatus(HttpStatus.UNAUTHORIZED).responseBody(auth).build();
        }

        GenericResponse myUserDetailsResponse = userService.getUserDetails(request.getHeader("username"));
        if (myUserDetailsResponse.getResult().getCode() == ActionResultCode.OK) {
            Tweet tweet = new Tweet();
            tweet.setId(tweetId);

            User user = (User) myUserDetailsResponse.getDetails();
            tweet.setUser(user);

            GenericResponse genericResponse = tweetService.unlikeTweet(tweet);
            return new ResponseEntityBuilder<GenericResponse>().setHttpStatus(HttpStatus.OK).responseBody(genericResponse).build();
        } else {
            GenericResponse genericResponse = new GenericResponse<>(ActionResult.buildResult(ActionResultCode.USERNAME_ERROR, "User not found."), null);
            return new ResponseEntityBuilder<GenericResponse>().setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR).responseBody(genericResponse).build();
        }
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public ResponseEntity<GenericResponse> searchTweets(HttpServletRequest request, @RequestParam(value = "value") String searchString) throws IOException {
        GenericResponse auth = apiAuthentication.attemptAuthentication(request);
        if (auth.getResult().getCode().equals(ActionResultCode.AUTH_FAILURE)) {
            return new ResponseEntityBuilder<GenericResponse>().setHttpStatus(HttpStatus.UNAUTHORIZED).responseBody(auth).build();
        }

        GenericResponse searchResult = tweetService.searchTweets(searchString);
        return new ResponseEntityBuilder<GenericResponse>().setHttpStatus(HttpStatus.OK).responseBody(searchResult).build();
    }

}
