package com.andrei.licenta.mvc.model;

/**
 * MIT License
 * <p>
 * Copyright (c) 2018 Andrei Popescu
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
public enum ActionResultCode {
    OK,
    ERROR,
    AUTH_FAILURE,
    AUTH_SUCCESS,
    USER_CREATION_SUCCESS,
    USER_CREATION_FAIL,
    USERNAME_ERROR,
    EMAIL_ERROR,
    PASSWORD_ERROR,
    TWEET_LIKE_DUPLICATE,
    TWEET_LIKE_SUCCESS,
    TWEET_LIKE_FAILURE,
    TWEET_UNLIKE_DUPLICATE,
    TWEET_UNLIKE_SUCCESS,
    TWEET_UNLIKE_FAILURE,
    USER_FOLLOW_DUPLICATE,
    USER_FOLLOW_SUCCESS,
    USER_FOLLOW_FAILURE,
    USER_UNFOLLOW_DUPLICATE,
    USER_UNFOLLOW_SUCCESS,
    USER_UNFOLLOW_FAILURE
}
