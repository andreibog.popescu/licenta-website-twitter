package com.andrei.licenta.handler;

import com.andrei.licenta.mvc.model.ActionResult;
import com.andrei.licenta.mvc.model.ActionResultCode;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.Set;

/**
 * MIT License
 * <p>
 * Copyright (c) 2018 Andrei Popescu
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
@Component
public class PasswordValidationHandler implements ValidationHandler {

    private String username;
    private String password;
    private String passwordMatching;

    public PasswordValidationHandler() {
    }

    public void setPasswordAndUsername(String username, String password, String passwordMatching) {
        actionResults.clear();
        this.username = username;
        this.password = password;
        this.passwordMatching = passwordMatching;
    }

    private void validatePassword() {

        if (password.isEmpty() || passwordMatching.isEmpty()) {
            actionResults.add(ActionResult.buildResult(ActionResultCode.PASSWORD_ERROR, "Password or matching password cannot be empty."));
            return;
        }
        if (!Objects.equals(password, passwordMatching)) {
            actionResults.add(ActionResult.buildResult(ActionResultCode.PASSWORD_ERROR, "Retyped password does not match provided password."));
        }
        if (password.length() > 20) {
            actionResults.add(ActionResult.buildResult(ActionResultCode.PASSWORD_ERROR, "Password should be less than 20 characters in length."));
        }
        if (password.length() < 8) {
            actionResults.add(ActionResult.buildResult(ActionResultCode.PASSWORD_ERROR, "Password should be more than 8 characters in length."));
        }
        if (password.toLowerCase().contains(username.toLowerCase()) || username.toLowerCase().contains(password.toLowerCase())) {
            actionResults.add(ActionResult.buildResult(ActionResultCode.PASSWORD_ERROR, "Password should not be the same or contain the username."));
        }
        if (!password.matches("(.*[A-Z].*)")) {
            actionResults.add(ActionResult.buildResult(ActionResultCode.PASSWORD_ERROR, "Password should contain at least one upper case alphabetic character."));
        }
        if (!password.matches("(.*[a-z].*)")) {
            actionResults.add(ActionResult.buildResult(ActionResultCode.PASSWORD_ERROR, "Password should contain at least one lower case alphabetic character."));
        }
        if (!password.matches("(.*[0-9].*)")) {
            actionResults.add(ActionResult.buildResult(ActionResultCode.PASSWORD_ERROR, "Password should contain at least one numeric character."));
        }
        if (!password.matches("(.*(?![0-9])[,~!@#$%^&*()-_=+[{]}|;:<>/?].*)")) {
            actionResults.add(ActionResult.buildResult(ActionResultCode.PASSWORD_ERROR, "Password should contain at least one special character."));
        }
    }

    @Override
    public Set<ActionResult> handleValidation() {
        validatePassword();

        return actionResults;
    }
}
