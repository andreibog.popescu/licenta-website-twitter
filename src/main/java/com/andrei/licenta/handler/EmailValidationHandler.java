package com.andrei.licenta.handler;

import com.andrei.licenta.mvc.model.ActionResult;
import com.andrei.licenta.mvc.model.ActionResultCode;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * MIT License
 * <p>
 * Copyright (c) 2018 Andrei Popescu
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
@Component
public class EmailValidationHandler implements ValidationHandler {

    /**
     * We should only check that an email address contains
     * 1. At most a single '@'
     * 2. The domain level does not contain numeric characters
     * 3. The name contains only alphanumeric characters
     * <p>
     * Even if this a very strict way to check a valid email, it should help in determining a limited
     * number of test cases.
     */
    private static final String EMAIL_REGEX_PATTERN = "[a-zA-Z0-9]+@[a-zA-Z]+\\.[a-zA-Z]+";
    private Pattern pattern;
    private Matcher matcher;
    private String email;

    public EmailValidationHandler() {
    }

    public void setEmail(String email) {
        actionResults.clear();
        this.email = email;
    }

    private boolean isValid() {
        if (email == null) {
            return false;
        }
        pattern = Pattern.compile(EMAIL_REGEX_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    @Override
    public Set<ActionResult> handleValidation() {
        if (!isValid()) {
            actionResults.add(ActionResult.buildResult(ActionResultCode.EMAIL_ERROR, "Invalid Email."));
        }

        return actionResults;
    }

}
