package com.andrei.licenta.service;

import com.andrei.licenta.db.dao.TweetDAO;
import com.andrei.licenta.db.dao.TweetLikeCountDAO;
import com.andrei.licenta.db.dao.TweetLikeDAO;
import com.andrei.licenta.db.dto.*;
import com.andrei.licenta.mvc.model.ActionResult;
import com.andrei.licenta.mvc.model.ActionResultCode;
import com.andrei.licenta.mvc.model.GenericResponse;
import com.andrei.licenta.mvc.model.GenericResponseBuilder;
import com.andrei.licenta.utils.LogProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * MIT License
 * <p>
 * Copyright (c) 2018 Andrei Popescu
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
@Service
public class TweetService implements LogProvider {

    @Autowired
    private TweetDAO tweets;

    @Autowired
    private UserService userService;

    @Autowired
    private TweetLikeDAO tweetLikes;

    @Autowired
    private TweetLikeCountDAO tweetLikesCount;

    @SuppressWarnings("unchecked")
    public GenericResponse getAllTweetsForUser(User myUser, User user) {

        logger().debug("Current User: {}", myUser);
        logger().info("Retrieve tweets for user: {}", user);

        List<Tweet> tweetList = tweets.findByUserOrderByCreationDateDesc(user);
        if (myUser == user){
            logger().info("Retrieve tweets from users that [{}] is following", myUser.getUsername());
            List<Follower> followerList = (List<Follower>) userService.getFollowing(myUser).getDetails();
            List<Tweet> tweetListWithFollowerTweets = tweetList;
            followerList.forEach(follower -> {
                if (follower.isActive()) {
                    tweetListWithFollowerTweets.addAll(tweets.findByUserOrderByCreationDateDesc(follower.getUserFollowing()));
                }
            });
            tweetList = tweetListWithFollowerTweets.stream().sorted(Comparator.comparing(Tweet::getCreationDate).reversed()).collect(Collectors.toList());
        }

        logger().debug("Adding if tweet is liked by current user and overall like count to retrieved tweets");
        tweetList.forEach(tweet -> {
            Optional<TweetLike> tweetLikeOpt = Optional.ofNullable(tweetLikes.findByTweetAndUser(tweet, myUser));
            if (tweetLikeOpt.isPresent() && tweetLikeOpt.get().isActive()){
                tweet.setLiked(true);
            }

            Optional<TweetLikeCount> tweetLikeCount = Optional.ofNullable(tweetLikesCount.findByTweet(tweet));
            if (tweetLikeCount.isPresent()){
                tweet.setLikeCount(tweetLikeCount.get().getLikeCount());
            } else {
                tweet.setLikeCount(0);
            }
        });

        logger().info("Tweets retrieved - Responding back");
        return new GenericResponseBuilder<List<Tweet>>()
                .addResult(ActionResult.buildResult(ActionResultCode.OK, "Tweets retrieved."))
                .addDetails(tweetList)
                .build();
    }

    public GenericResponse addTweet(Tweet tweetDetails) {
        logger().info("Add tweet '{}' for user: {}", tweetDetails.getContent(), tweetDetails.getUser());
        tweetDetails = tweets.save(tweetDetails);

        logger().info("Tweet added successfully.");
        return new GenericResponseBuilder<Tweet>()
                .addResult(ActionResult.buildResult(ActionResultCode.OK, "Tweet Added."))
                .addDetails(tweetDetails)
                .build();
    }

    public GenericResponse likeTweet(Tweet tweetDetails) {
        logger().info("Liking tweet with id {} for user: {}", tweetDetails.getId(), tweetDetails.getUser());
        Optional<TweetLike> tweetLikesOptional = Optional.ofNullable(
                tweetLikes.findByTweetAndUser(
                        tweetDetails,
                        tweetDetails.getUser()
                )
        );

        if (tweetLikesOptional.isPresent()) {
            TweetLike tweetLike = tweetLikesOptional.get();
            if (tweetLike.isActive()) {
                logger().info("Tweet is already liked");
                return new GenericResponseBuilder<>()
                        .addResult(ActionResult.buildResult(ActionResultCode.TWEET_LIKE_DUPLICATE, "Tweet already liked."))
                        .build();
            } else {
                logger().debug("Activating tweet like");
                tweetLike.setActive(true);
                tweetLikes.save(tweetLike);
                logger().debug("Incrementing tweet like count");
                tweetLikesCount.incrementLikeCountByTweetId(tweetLike.getTweet());

                logger().info("Tweet liked successfully");
                return new GenericResponseBuilder<>()
                        .addResult(ActionResult.buildResult(ActionResultCode.TWEET_LIKE_SUCCESS, "Tweet like successful."))
                        .build();
            }
        } else {
            Optional<Tweet> tweet = tweets.findById(tweetDetails.getId());

            if (!tweet.isPresent()) {
                logger().info("Tweet does not exist");
                return new GenericResponseBuilder<>()
                        .addResult(ActionResult.buildResult(ActionResultCode.TWEET_LIKE_FAILURE, "Tweet does not exist."))
                        .build();
            } else {
                TweetLike tweetLike = new TweetLike(tweetDetails.getUser(), tweet.get());
                tweetLike.setActive(true);

                logger().debug("Activating tweet like");
                tweetLikes.save(tweetLike);

                Optional<TweetLikeCount> tweetLikeCountOpt = Optional.ofNullable(tweetLikesCount.findByTweet(tweet.get()));
                logger().debug("Incrementing tweet like count");
                if (!tweetLikeCountOpt.isPresent()) {
                    TweetLikeCount tweetLikeCount = new TweetLikeCount(tweet.get(), 1);
                    tweetLikesCount.save(tweetLikeCount);
                } else {
                    tweetLikesCount.incrementLikeCountByTweetId(tweet.get());
                }

                logger().info("Tweet liked successfully");
                return new GenericResponseBuilder<>()
                        .addResult(ActionResult.buildResult(ActionResultCode.TWEET_LIKE_SUCCESS, "Tweet like successful."))
                        .build();
            }
        }

    }

    public GenericResponse unlikeTweet(Tweet tweetDetails) {
        logger().info("Unliking tweet with id {} for user: {}", tweetDetails.getId(), tweetDetails.getUser());
        Optional<TweetLike> tweetLikesOptional = Optional.ofNullable(
                tweetLikes.findByTweetAndUser(
                        tweetDetails,
                        tweetDetails.getUser()
                )
        );

        if (tweetLikesOptional.isPresent()) {
            TweetLike tweetLike = tweetLikesOptional.get();
            if (!tweetLike.isActive()) {
                logger().info("Tweet is already unliked");
                return new GenericResponseBuilder<>()
                        .addResult(ActionResult.buildResult(ActionResultCode.TWEET_UNLIKE_DUPLICATE, "Tweet already unlike."))
                        .build();
            } else {
                logger().debug("Deactivating tweet like");
                tweetLike.setActive(false);
                tweetLikes.save(tweetLike);
                logger().debug("Decrementing tweet like count");
                tweetLikesCount.decrementLikeCountByTweetId(tweetLike.getTweet());

                return new GenericResponseBuilder<>()
                        .addResult(ActionResult.buildResult(ActionResultCode.TWEET_UNLIKE_SUCCESS, "Tweet unlike successful."))
                        .build();
            }
        } else {
            logger().info("Tweet needs to be liked first");
            return new GenericResponseBuilder<>()
                    .addResult(ActionResult.buildResult(ActionResultCode.TWEET_UNLIKE_FAILURE, "Tweet needs to be liked first."))
                    .build();
        }
    }

    public GenericResponse searchTweets(String searchString) {
        logger().info("Searching for tweets with partial matching the following string: {}", searchString);
        List<Tweet> searchResult = tweets.findByContentContainingIgnoreCaseOrderByCreationDateDesc(searchString);

        logger().info("Retrieving search result");
        return new GenericResponseBuilder<List<Tweet>>()
                .addResult(ActionResult.buildResult(ActionResultCode.OK, "Search result retrieved."))
                .addDetails(searchResult)
                .build();
    }
}
