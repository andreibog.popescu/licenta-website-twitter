package com.andrei.licenta.service;

import com.andrei.licenta.auth.configuration.LoginAuthenticationProvider;
import com.andrei.licenta.db.dto.Follower;
import com.andrei.licenta.db.dto.Tweet;
import com.andrei.licenta.db.dto.User;
import com.andrei.licenta.mvc.model.ActionResult;
import com.andrei.licenta.mvc.model.ActionResultCode;
import com.andrei.licenta.mvc.model.GenericResponse;
import com.andrei.licenta.mvc.model.RegisterUser;
import com.andrei.licenta.utils.GlobalMessageUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class InterfaceService {

    @Autowired
    private UserService userService;

    @Autowired
    private ApiRequestService requestService;

    @Autowired
    private LoginAuthenticationProvider loginAuthenticationProvider;

    @Autowired
    private GlobalMessageUtil globalMessageUtil;

    private ObjectMapper jacksonMapper = new ObjectMapper();

    public String redirectToProfile(Model model) {
        return redirectToProfile(model, null);
    }

    public String redirectToProfile(Model model, String username) {
        User currentUser = setupProfile(model);

        if (username == null || username.isEmpty()) {
            return "redirect:/u/" + currentUser.getUsername();
        }
        return "redirect:/u/" + username;
    }

    private User setupProfile(Model model) {
        User currentUser = (User) userService.getUserDetails(getAuth().getName()).getDetails();
        model.addAttribute("currentUser", currentUser);

        return currentUser;
    }

    public Authentication getAuth() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    private void setAuth(String email, String password) {
        SecurityContextHolder.getContext().setAuthentication(
                loginAuthenticationProvider.authenticate(
                        new UsernamePasswordAuthenticationToken(email, password)
                )
        );
    }

    public boolean checkAnonymous() {
        return getAuth().getPrincipal().equals("anonymousUser");
    }

    private List<Tweet> getUserTweets(String username) {
        GenericResponse tweetsResponse = requestService.getUserTweets(username).getBody();
        List<Tweet> tweets = jacksonMapper.convertValue(tweetsResponse.getDetails(), new TypeReference<List<Tweet>>() {
        });
        return tweets;
    }

    private List<Follower> getUserFollowers(String username) {
        GenericResponse followersResponse = requestService.getUserFollowers(username).getBody();
        List<Follower> followers = jacksonMapper.convertValue(followersResponse.getDetails(), new TypeReference<List<Follower>>() {
        });
        return followers;
    }

    public String registerUser(HttpServletRequest request, Model model) {
        RegisterUser user = new RegisterUser(
                request.getParameter("username"),
                request.getParameter("password"),
                request.getParameter("matchingPassword"),
                request.getParameter("email")
        );

        GenericResponse registerResponse = requestService.registerUserRequest(user).getBody();

        if (registerResponse.getResult().getCode() != ActionResultCode.USER_CREATION_SUCCESS) {
            List<ActionResult> errorDetails = jacksonMapper.convertValue(registerResponse.getDetails(), new TypeReference<List<ActionResult>>() {
            });

            errorDetails
                    .stream()
                    .collect(Collectors.groupingBy(ActionResult::getCode))
                    .forEach((actionResultCode, actionResults) -> {
                        switch (actionResultCode) {
                            case USERNAME_ERROR:
                                model.addAttribute("errorUsername", true);
                                model.addAttribute("errorUsernameMessages", actionResults.stream().map(ActionResult::getReason).collect(Collectors.toList()));
                                break;
                            case EMAIL_ERROR:
                                model.addAttribute("errorEmail", true);
                                model.addAttribute("errorEmailMessages", actionResults.stream().map(ActionResult::getReason).collect(Collectors.toList()));
                                break;
                            case PASSWORD_ERROR:
                                model.addAttribute("errorPassword", true);
                                model.addAttribute("errorPasswordMessages", actionResults.stream().map(ActionResult::getReason).collect(Collectors.toList()));
                                break;
                            default:
                                break;
                        }
                    });

            model.addAttribute("username", user.getUsername());
            model.addAttribute("email", user.getEmail());

            return "register";
        }

        setAuth(user.getEmail(), user.getPassword());
        return "redirect:/u/" + user.getUsername();
    }

    public void checkGlobalErrors(Model model) {
        if (globalMessageUtil.getVariables().containsKey("error")) {
            model.addAttribute("error", globalMessageUtil.getVariables().get("error"));
            model.addAttribute("errorMessage", globalMessageUtil.getVariables().get("errorMessage"));
            globalMessageUtil.clearVariables();
        }
    }

    public boolean checkError(Model model, GenericResponse response, ActionResultCode expectedCode) {
        if (response != null && response.getResult().getCode() == expectedCode) {
            model.addAttribute("error", true);
            model.addAttribute("errorMessage", response.getResult().getReason());
            return true;
        }
        return false;
    }

    public void clearAuth() {
        SecurityContextHolder.clearContext();
    }

    public String displayProfile(Model model, String username) {
        setupProfile(model);

        GenericResponse displayProfileResponse = requestService.getUserRequest(username).getBody();

        if (checkError(model, displayProfileResponse, ActionResultCode.ERROR)) {
            return "profile";
        }

        User user = jacksonMapper.convertValue(displayProfileResponse.getDetails(), User.class);
        List<Tweet> tweets = getUserTweets(username);
        List<Follower> followers = getUserFollowers(username);

        model.addAttribute("profile", user);
        model.addAttribute("tweets", tweets);
        model.addAttribute("followers", followers);

        return "profile";
    }

    public String postTweet(HttpServletRequest request, Model model) {
        User currentUser = setupProfile(model);

        Tweet tweet = new Tweet();
        tweet.setContent(request.getParameter("content"));

        GenericResponse postTweetResponse = requestService.postTweet(currentUser.getUsername(), tweet).getBody();

        checkError(model, postTweetResponse, ActionResultCode.ERROR);

        return redirectToProfile(model);
    }

    public String editProfileDescription(HttpServletRequest request, Model model) {
        User currentUser = setupProfile(model);

        User user = new User();
        user.setDescription(request.getParameter("description"));

        GenericResponse registerResponse = requestService.putProfile(currentUser.getUsername(), user).getBody();

        checkError(model, registerResponse, ActionResultCode.ERROR);

        return redirectToProfile(model);
    }

    public String likeTweet(HttpServletRequest request, Model model) {
        int tweetId = Integer.parseInt(request.getParameter("tweetId"));
        String profileUsername = request.getParameter("profileUsername");

        GenericResponse tweetLikeResponse = requestService.likeTweet(tweetId).getBody();
        if (checkError(model, tweetLikeResponse, ActionResultCode.ERROR)) {
            return redirectToProfile(model);
        }

        if (tweetLikeResponse.getResult().getCode() == ActionResultCode.TWEET_LIKE_DUPLICATE) {
            GenericResponse tweetUnLikeResponse = requestService.unlikeTweet(tweetId).getBody();

            checkError(model, tweetUnLikeResponse, ActionResultCode.ERROR);
            checkError(model, tweetUnLikeResponse, ActionResultCode.TWEET_UNLIKE_FAILURE);
        } else {
            checkError(model, tweetLikeResponse, ActionResultCode.TWEET_LIKE_FAILURE);
        }

        return redirectToProfile(model, profileUsername);
    }

    public String followUser(HttpServletRequest request, Model model) {
        int followerId = Integer.parseInt(request.getParameter("followerId"));
        String followerUsername = request.getParameter("followerUsername");

        GenericResponse followUserResponse = requestService.followUser(followerId).getBody();
        if (checkError(model, followUserResponse, ActionResultCode.ERROR)) {
            return redirectToProfile(model);
        }

        if (followUserResponse.getResult().getCode() == ActionResultCode.USER_FOLLOW_DUPLICATE) {
            GenericResponse unfollowUserResponse = requestService.unfollowUser(followerId).getBody();

            checkError(model, unfollowUserResponse, ActionResultCode.ERROR);
            checkError(model, unfollowUserResponse, ActionResultCode.USER_UNFOLLOW_FAILURE);
        } else {
            checkError(model, followUserResponse, ActionResultCode.USER_FOLLOW_FAILURE);
        }

        return redirectToProfile(model, followerUsername);
    }

    public String search(HttpServletRequest request, Model model) {
        setupProfile(model);

        String searchValue = request.getParameter("search");

        GenericResponse userResponse = requestService.searchUsers(searchValue).getBody();
        List<User> usersFound = jacksonMapper.convertValue(userResponse.getDetails(),  new TypeReference<List<User>>() {});

        GenericResponse tweetResponse = requestService.searchTweets(searchValue).getBody();
        List<Tweet> tweetsFound = jacksonMapper.convertValue(tweetResponse.getDetails(),  new TypeReference<List<Tweet>>() {});

        model.addAttribute("users", usersFound);
        model.addAttribute("tweets", tweetsFound);

        return "search";
    }

    public String showSearch(Model model) {
        setupProfile(model);

        return "search";
    }
}

