package com.andrei.licenta.service;

import com.andrei.licenta.db.dto.Tweet;
import com.andrei.licenta.db.dto.User;
import com.andrei.licenta.mvc.model.GenericResponse;
import com.andrei.licenta.mvc.model.RegisterUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ApiRequestService {

    @Autowired
    private RestTemplate restOperations;

    @Value("${api.url:http://localhost:8080}")
    private String apiUrl;

    public ResponseEntity<GenericResponse> registerUserRequest(RegisterUser user) {
        return restOperations.postForEntity(formatUrl(RequestUrl.REGISTER_USER), user, GenericResponse.class);
    }

    public ResponseEntity<GenericResponse> getUserRequest(String username) {
        return restOperations.exchange(formatUrl(RequestUrl.USER_DETAILS, username), HttpMethod.GET, setAuthenticationHeaders(""), GenericResponse.class);
    }

    public ResponseEntity<GenericResponse> getUserTweets(String username) {
        return restOperations.exchange(formatUrl(RequestUrl.USER_TWEETS, username), HttpMethod.GET, setAuthenticationHeaders(""), GenericResponse.class);
    }

    public ResponseEntity<GenericResponse> getUserFollowers(String username) {
        return restOperations.exchange(formatUrl(RequestUrl.USER_FOLLOWERS, username), HttpMethod.GET, setAuthenticationHeaders(""), GenericResponse.class);
    }

    public ResponseEntity<GenericResponse> postTweet(String username, Tweet tweet) {
        return restOperations.exchange(formatUrl(RequestUrl.USER_TWEETS, username), HttpMethod.POST, setAuthenticationHeaders(tweet), GenericResponse.class);
    }

    public ResponseEntity<GenericResponse> putProfile(String username, User user) {
        return restOperations.exchange(formatUrl(RequestUrl.USER_DETAILS, username), HttpMethod.PUT, setAuthenticationHeaders(user), GenericResponse.class);
    }

    public ResponseEntity<GenericResponse> likeTweet(int tweetId){
        return restOperations.exchange(formatUrl(RequestUrl.LIKE_TWEET, tweetId), HttpMethod.POST, setAuthenticationHeaders(""), GenericResponse.class);
    }

    public ResponseEntity<GenericResponse> unlikeTweet(int tweetId){
        return restOperations.exchange(formatUrl(RequestUrl.UNLIKE_TWEET, tweetId), HttpMethod.POST, setAuthenticationHeaders(""), GenericResponse.class);
    }

    public ResponseEntity<GenericResponse> followUser(int userId){
        return restOperations.exchange(formatUrl(RequestUrl.FOLLOW_USER, userId), HttpMethod.POST, setAuthenticationHeaders(""), GenericResponse.class);
    }

    public ResponseEntity<GenericResponse> unfollowUser(int userId){
        return restOperations.exchange(formatUrl(RequestUrl.UNFOLLOW_USER, userId), HttpMethod.POST, setAuthenticationHeaders(""), GenericResponse.class);
    }

    public ResponseEntity<GenericResponse> searchUsers(String searchValue){
        return restOperations.exchange(formatUrl(RequestUrl.SEARCH_USERS, searchValue), HttpMethod.GET, setAuthenticationHeaders(""), GenericResponse.class);
    }

    public ResponseEntity<GenericResponse> searchTweets(String searchValue){
        return restOperations.exchange(formatUrl(RequestUrl.SEARCH_TWEETS, searchValue), HttpMethod.GET, setAuthenticationHeaders(""), GenericResponse.class);
    }

    private <T> HttpEntity<T> setAuthenticationHeaders(T body) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        HttpHeaders headers = new HttpHeaders();
        headers.add("username", authentication.getName());
        headers.add("password", authentication.getCredentials().toString());

        HttpEntity<T> entity = new HttpEntity<>(body, headers);

        return entity;
    }

    private String formatUrl(String path, Object... args) {
        return String.format(apiUrl + path, (Object[]) args);
    }

    public interface RequestUrl {
        String REGISTER_USER = "/api/user/add";
        String USER_DETAILS = "/api/user/%s";
        String USER_TWEETS = "/api/tweet/%s";
        String USER_FOLLOWERS = "/api/user/%s/followers";
        String LIKE_TWEET = "/api/tweet/%s/like";
        String UNLIKE_TWEET = "/api/tweet/%d/unlike";
        String FOLLOW_USER = "/api/user/%d/follow";
        String UNFOLLOW_USER = "/api/user/%d/unfollow";
        String SEARCH_USERS = "/api/user/search?value=%s";
        String SEARCH_TWEETS = "/api/tweet/search?value=%s";
    }

}
