package com.andrei.licenta.service;

import com.andrei.licenta.auth.PasswordEncoder;
import com.andrei.licenta.db.dao.FollowerDAO;
import com.andrei.licenta.db.dao.RoleDAO;
import com.andrei.licenta.db.dao.UserDAO;
import com.andrei.licenta.db.dto.Follower;
import com.andrei.licenta.db.dto.Role;
import com.andrei.licenta.db.dto.User;
import com.andrei.licenta.handler.EmailValidationHandler;
import com.andrei.licenta.handler.PasswordValidationHandler;
import com.andrei.licenta.mvc.model.*;
import com.andrei.licenta.utils.LogProvider;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * MIT License
 * <p>
 * Copyright (c) 2018 Andrei Popescu
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
@Service
public class  UserService implements LogProvider {

    @Autowired
    private UserDAO users;

    @Autowired
    private RoleDAO roles;

    @Autowired
    private FollowerDAO followers;

    @Autowired
    private EmailValidationHandler emailValidationHandler;

    @Autowired
    private PasswordValidationHandler passwordValidationHandler;

    public GenericResponse searchUsers(String searchString) {
        logger().info("Searching for users with partial matching the following string: {}", searchString);
        List<User> searchResult = users.findByUsernameContainingIgnoreCaseOrderByUsernameAsc(searchString);

        logger().info("Retrieving search result");
        return new GenericResponseBuilder<List<User>>()
                .addResult(ActionResult.buildResult(ActionResultCode.OK, "Search result retrieved."))
                .addDetails(searchResult)
                .build();
    }

    public GenericResponse editUser(User user) {
        logger().info("Saving new user details");
        user = users.save(user);

        logger().info("User details saved");
        return new GenericResponseBuilder<User>()
                .addResult(ActionResult.buildResult(ActionResultCode.OK, "User edited."))
                .addDetails(user)
                .build();
    }

    public GenericResponse getUserDetails(Long userId) {
        logger().info("Retrieving user details for user with id {}", userId);
        Optional<User> userOptional = users.findById(userId);

        GenericResponseBuilder<User> responseBuilder = new GenericResponseBuilder<>();

        responseBuilder.addResult(ActionResult.buildResult(ActionResultCode.ERROR, "This user does not exist"));

        userOptional.ifPresent(entity -> responseBuilder
                .addResult(ActionResult.buildResult(ActionResultCode.OK, "User details provided."))
                .addDetails(entity));

        return responseBuilder.build();
    }

    public GenericResponse getUserDetails(String user) {
        Optional<User> userOptional = Optional.ofNullable(users.findByUsername(user));
        if (!userOptional.isPresent()) {
            logger().info("Retrieving user details for email {}", user);
            userOptional = Optional.ofNullable(users.findByEmail(user));
        } else {
            logger().info("Retrieving user details for username {}", user);
        }

        GenericResponseBuilder<User> responseBuilder = new GenericResponseBuilder<>();

        responseBuilder.addResult(ActionResult.buildResult(ActionResultCode.ERROR, "This user does not exist"));

        userOptional.ifPresent(entity -> responseBuilder
                .addResult(ActionResult.buildResult(ActionResultCode.OK, "User details provided."))
                .addDetails(entity));

        return responseBuilder.build();
    }

    public GenericResponse registerUser(RegisterUser user) throws JsonProcessingException {
        logger().info("Registering user with details: {}", user);
        Set<ActionResult> actionResults = new LinkedHashSet<>();
        emailValidationHandler.setEmail(user.getEmail());
        passwordValidationHandler.setPasswordAndUsername(user.getUsername(), user.getPassword(), user.getMatchingPassword());

        logger().info("Validating if empty username");
        if (user.getUsername().isEmpty()) {
            actionResults.add(ActionResult.buildResult(ActionResultCode.USERNAME_ERROR, "Invalid username."));
        }

        logger().info("Validating if existing username");
        Optional<User> existingUser = Optional.ofNullable(users.findByUsername(user.getUsername()));
        existingUser.ifPresent(userEntity -> actionResults.add(ActionResult.buildResult(ActionResultCode.USERNAME_ERROR, "User already exists.")));

        logger().info("Validating if existing email");
        existingUser = Optional.ofNullable(users.findByEmail(user.getEmail()));
        existingUser.ifPresent(userEntity -> actionResults.add(ActionResult.buildResult(ActionResultCode.EMAIL_ERROR, "Email has been registered to another user.")));

        logger().info("Validating email");
        actionResults.addAll(emailValidationHandler.handleValidation());
        logger().info("Validating password");
        actionResults.addAll(passwordValidationHandler.handleValidation());

        if (actionResults.isEmpty()) {
            logger().debug("Encrypting password");
            //Encrypt password
            user.setPassword(PasswordEncoder.encrypt(user.getPassword()));
            Role role = roles.findByRoleName("ROLE_USER");
            User userEntity = new User(user.getUsername(), user.getPassword(), user.getEmail(), role);

            if (!existingUser.isPresent()) {
                users.save(userEntity);
            }

            logger().info("User successfully registered");
            ActionResult actionResult = ActionResult.buildResult(ActionResultCode.USER_CREATION_SUCCESS, "User successfully registered.");
            return new GenericResponseBuilder()
                    .addResult(actionResult)
                    .build();
        } else {
            logger().info("User registration failed");
            ActionResult actionResult = ActionResult.buildResult(ActionResultCode.USER_CREATION_FAIL, "User registration failed.");
            return new GenericResponseBuilder<Set<ActionResult>>()
                    .addResult(actionResult)
                    .addDetails(actionResults)
                    .build();
        }

    }

    public GenericResponse getAllUsers() {
        logger().info("Retrieving all users");
        return new GenericResponseBuilder<List<User>>()
                .addResult(ActionResult.buildResult(ActionResultCode.OK, "All users retrieved."))
                .addDetails(users.findAll())
                .build();
    }

    public GenericResponse followUser(Follower followerObj){
        logger().info("User follow: [{}] -> [{}]", followerObj.getUser().getUsername(), followerObj.getUserFollowing().getUsername());
        Optional<Follower> existingFollowerObjOptional = Optional.ofNullable(
                followers.findByUserAndUserFollowing(
                        followerObj.getUser(),
                        followerObj.getUserFollowing()
                )
        );

        if (existingFollowerObjOptional.isPresent()){
            Follower existingFollowerObj = existingFollowerObjOptional.get();
            if (!existingFollowerObj.isActive()){

                logger().info("Update follow date and activate follow");
                existingFollowerObj.setFollowDate(Timestamp.from(Instant.now()));
                existingFollowerObj.setActive(true);
                followers.save(existingFollowerObj);

                logger().info("User follow successful");
                return new GenericResponseBuilder<>()
                        .addResult(ActionResult.buildResult(ActionResultCode.USER_FOLLOW_SUCCESS, "User follow successful."))
                        .build();
            } else {
                logger().info("User already followed");
                return new GenericResponseBuilder<>()
                        .addResult(ActionResult.buildResult(ActionResultCode.USER_FOLLOW_DUPLICATE, "User already followed."))
                        .build();
            }
        } else {
            logger().info("Update follow date and activate follow");
            followerObj.setFollowDate(Timestamp.from(Instant.now()));
            followerObj.setActive(true);
            followers.save(followerObj);

            logger().info("User follow successful");
            return new GenericResponseBuilder<>()
                    .addResult(ActionResult.buildResult(ActionResultCode.USER_FOLLOW_SUCCESS, "User follow successful."))
                    .build();
        }
    }

    public GenericResponse unFollowUser(Follower followerObj){
        logger().info("User unfollow: [{}] -> [{}]", followerObj.getUser().getUsername(), followerObj.getUserFollowing().getUsername());
        Optional<Follower> existingFollowerObjOptional = Optional.ofNullable(
                followers.findByUserAndUserFollowing(
                        followerObj.getUser(),
                        followerObj.getUserFollowing()
                )
        );

        if (existingFollowerObjOptional.isPresent()){
            Follower existingFollowerObj = existingFollowerObjOptional.get();
            if (existingFollowerObj.isActive()){
                logger().info("Deactivate follow");
                existingFollowerObj.setActive(false);
                followers.save(existingFollowerObj);

                logger().info("User unfollow successful");
                return new GenericResponseBuilder<>()
                        .addResult(ActionResult.buildResult(ActionResultCode.USER_UNFOLLOW_SUCCESS, "User unfollow successful."))
                        .build();
            } else {
                logger().info("User already unfollowed");
                return new GenericResponseBuilder<>()
                        .addResult(ActionResult.buildResult(ActionResultCode.USER_UNFOLLOW_DUPLICATE, "User already unfollowed."))
                        .build();
            }
        } else {
            logger().info("User must be followed first");
            return new GenericResponseBuilder<>()
                    .addResult(ActionResult.buildResult(ActionResultCode.USER_UNFOLLOW_FAILURE, "User must be followed first."))
                    .build();
        }
    }

    public GenericResponse getFollowing(User user){
        logger().info("Getting all following for user {}", user.getUsername());
        List<Follower> followerList = followers.findByUser(user);

        return new GenericResponseBuilder<>()
                .addResult(ActionResult.buildResult(ActionResultCode.OK, "Retrieved user followings."))
                .addDetails(followerList)
                .build();
    }

    public GenericResponse getFollowers(User myUser, User user) {
        logger().info("Getting all followers for user {}", user.getUsername());
        List<Follower> followerList = followers.findByUserFollowingAndActiveTrueOrderByFollowDateDesc(user);

        followerList.forEach(follower -> {
            Optional<Follower> myUserFollowerOpt = Optional.ofNullable(
                    followers.findByUserAndUserFollowing(myUser, follower.getUser())
            );

            if (myUserFollowerOpt.isPresent() && myUserFollowerOpt.get().isActive()){
                follower.getUser().setFollowed(true);
            } else {
                follower.getUser().setFollowed(false);
            }
        });

        return new GenericResponseBuilder<>()
                .addResult(ActionResult.buildResult(ActionResultCode.OK, "Retrieved user followers."))
                .addDetails(followerList)
                .build();
    }

    public boolean checkIfFollowed(User myUser, User user) {
        logger().info("Checking follow: [{}] -> [{}]", myUser.getUsername(), user.getUsername());
        Optional<Follower> followerOptional = Optional.ofNullable(followers.findByUserAndUserFollowing(myUser, user));

        boolean result = followerOptional.isPresent() && followerOptional.get().isActive();
        logger().debug("Check follow result: {}", result);

        return result;
    }

    @Profile("dev")
    public void deleteTestUsers() {
        logger().debug("Deleting test users");

        List<User> testUserList = users.findByUsernameContainingIgnoreCaseOrderByUsernameAsc("TestUser");

        testUserList.forEach(user -> users.delete(user));
    }
}
