package com.andrei.licenta.auth.configuration;

import com.andrei.licenta.auth.PasswordEncoder;
import com.andrei.licenta.db.dao.UserDAO;
import com.andrei.licenta.db.dto.Role;
import com.andrei.licenta.db.dto.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * MIT License
 * <p>
 * Copyright (c) 2018 Andrei Popescu
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
@Component
public class LoginAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private UserDAO usersDAO;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        if (authentication.getPrincipal() == null || authentication.getCredentials() == null) {
            throw new AuthenticationServiceException("Email or Password must not be empty.");
        }

        Optional<User> userFound = Optional.ofNullable(usersDAO.findByEmail(authentication.getPrincipal().toString()));

        if (userFound.isPresent()) {
            if (PasswordEncoder.isMatching(authentication.getCredentials().toString(), userFound.get().getPassword())) {
                List<GrantedAuthority> grantedAuthorities = parseGrantedAuthentications(userFound.get().getRoleId());

                if (grantedAuthorities.isEmpty()) {
                    throw new InsufficientAuthenticationException("Invalid email or password.");
                }
                return new UsernamePasswordAuthenticationToken(userFound.get().getEmail(), authentication.getCredentials().toString(), grantedAuthorities);
            } else {
                throw new BadCredentialsException("Invalid email or password.");
            }
        } else {
            throw new UsernameNotFoundException("Invalid email or password.");
        }

    }

    @Override
    public boolean supports(Class<?> authentication) {
        return (UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication));
    }

    private List<GrantedAuthority> parseGrantedAuthentications(Role role) {
        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();

        switch (role.getAuthority()) {
            case "ROLE_ADMIN":
                grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_USER"));
                grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
                break;
            case "ROLE_USER":
                grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_USER"));
                break;
            default:
                break;
        }

        return grantedAuthorities;
    }
}
