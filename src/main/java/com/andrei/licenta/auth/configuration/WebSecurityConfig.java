package com.andrei.licenta.auth.configuration;

import com.andrei.licenta.utils.GlobalMessageUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableGlobalMethodSecurity(securedEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final LoginAuthenticationProvider loginAuthProvider;
    private final LoginFailureHandler loginFailureHandler;
    private GlobalMessageUtil messageUtil;

    @Autowired
    public WebSecurityConfig(LoginAuthenticationProvider loginAuthProvider, LoginFailureHandler loginFailureHandler, GlobalMessageUtil messageUtil) {
        this.loginAuthProvider = loginAuthProvider;
        this.loginFailureHandler = loginFailureHandler;
        this.messageUtil = messageUtil;
    }

    @Autowired
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(loginAuthProvider);
        auth.eraseCredentials(false);
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().antMatchers("/api/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        LoginProcessingFilter loginFilter = new LoginProcessingFilter("/login");
        loginFilter.setAuthenticationManager(this.authenticationManager());
        loginFilter.setAuthenticationFailureHandler(loginFailureHandler);
        loginFilter.setMessageUtil(messageUtil);

        http
                .csrf().disable()
                .formLogin()
                .loginPage("/login")
                .defaultSuccessUrl("/profile")
                .and()
                .exceptionHandling()
                .accessDeniedPage("/error")
                .and()
                .authorizeRequests()
                .antMatchers("/webjars/**", "/login", "/register").permitAll()
                .anyRequest().authenticated()
                .and()
                .exceptionHandling()
                .and()
                .addFilterBefore(loginFilter, UsernamePasswordAuthenticationFilter.class);

    }
}
