package com.andrei.licenta.db.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "tfollower")
public class Follower implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Long id;

    @OneToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "userId", referencedColumnName = "id")
    private User user;

    @OneToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "userFollowingId", referencedColumnName = "id")
    private User userFollowing;

    @Column(name = "followDate", columnDefinition = "timestamp default now()")
    @JsonFormat(pattern = "dd-MMM-yyyy HH:mm")
    private Timestamp followDate;

    @Column(name = "active", nullable = false, columnDefinition = "boolean default true")
    private boolean active;

    public Follower(){}

    public Follower(User user, User userFollowing) {
        this.user = user;
        this.userFollowing = userFollowing;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUserFollowing() {
        return userFollowing;
    }

    public void setUserFollowing(User userFollowing) {
        this.userFollowing = userFollowing;
    }

    public Timestamp getFollowDate() {
        return followDate;
    }

    public void setFollowDate(Timestamp followDate) {
        this.followDate = followDate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Follower follower = (Follower) o;
        return Objects.equals(user, follower.user) &&
                Objects.equals(userFollowing, follower.userFollowing);
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, userFollowing);
    }
}
