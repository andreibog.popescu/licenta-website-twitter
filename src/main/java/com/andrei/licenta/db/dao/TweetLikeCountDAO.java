package com.andrei.licenta.db.dao;

import com.andrei.licenta.db.dto.Tweet;
import com.andrei.licenta.db.dto.TweetLikeCount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface TweetLikeCountDAO extends JpaRepository<TweetLikeCount, Long> {

    TweetLikeCount findByTweet(Tweet tweet);

    @Modifying
    @Query("update TweetLikeCount u set u.likeCount = u.likeCount + 1 where u.tweet = ?1")
    @Transactional
    void incrementLikeCountByTweetId(Tweet tweetId);

    @Modifying
    @Query("update TweetLikeCount u set u.likeCount = u.likeCount - 1 where u.tweet = ?1")
    @Transactional
    void decrementLikeCountByTweetId(Tweet tweetId);
}
