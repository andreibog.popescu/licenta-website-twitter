package com.andrei.licenta.db.dao;

import com.andrei.licenta.db.dto.Follower;
import com.andrei.licenta.db.dto.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FollowerDAO extends JpaRepository<Follower, Long> {

    List<Follower> findByUserFollowingAndActiveTrueOrderByFollowDateDesc(User user);

    List<Follower> findByUser(User user);

    Follower findByUserAndUserFollowing(User user, User userFollowing);
}
